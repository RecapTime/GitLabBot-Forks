# frozen_string_literal: true

require 'yaml'
require 'net/http'

class WwwGitLabCom
  WWW_GITLAB_COM_STAGES_YML = 'https://gitlab.com/MadeByThePinsTeam-DevLabs/gitlab-deploys/raw/master/data/stages.yml'
  WWW_GITLAB_COM_CATEGORIES_YML = 'https://gitlab.com/MadeByThePinsTeam-DevLabs/gitlab-deploys/raw/master/data/categories.yml'

  def self.stages_from_www
    @stages_from_www ||= fetch_yml(WWW_GITLAB_COM_STAGES_YML)['stages']
  end

  def self.groups_from_www
    @groups_from_www ||=
      stages_from_www.each_with_object({}) do |(stage, attrs), memo|
        memo.merge!(attrs['groups'])
      end
  end

  def self.categories_from_www
    @categories_from_www ||= fetch_yml(WWW_GITLAB_COM_CATEGORIES_YML)
  end

  def self.fetch_yml(yaml_url)
    YAML.load(request_yaml(yaml_url))
  end
  private_class_method :fetch_yml

  def self.request_yaml(yaml_url)
    Net::HTTP.get(URI(yaml_url))
  end
  private_class_method :request_yaml
end
