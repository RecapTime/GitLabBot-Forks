# frozen_string_literal: true

require_relative File.expand_path('../lib/lazy_heat_map.rb', __dir__)

Gitlab::Triage::EntityBuilders::IssueBuilder.prepend(IssueBuilderWithHeatMap)
