# GitLab triage operations

*This is a prototype. The goal is to add immediate value to the the Pins team's Engineering function, while determining if it adds value to customers. [Soon to be available on GitLab as an product](https://gitlab.com/groups/gitlab-org/-/epics/636).*

Triage operations for GitLab issues and merge requests. This is powered by <https://gitlab.com/MadeByThePinsTeam-DevLabs/gitlab-triage>.

## The bot

We're using [@RecapTime_bot](https://gitlab.com/RecapTime_bot) as the user to run
triage operations. Contact our [IT admin](mailto:it-admin@madebythepins.tk) or [Bot Support](https://supportcentral-madebythepins.freshdesk.com/support/solutions/articles/47000478596).

The same bot is also used in the following projects:

## The schedules

### Triage operations schedules (daily)

* [CE daily triage](https://gitlab.com/gitlab-org/quality/triage-ops/pipeline_schedules/10512/edit)
* [EE daily triage](https://gitlab.com/gitlab-org/quality/triage-ops/pipeline_schedules/10515/edit)
* [Gitaly daily triage](https://gitlab.com/gitlab-org/quality/triage-ops/pipeline_schedules/29054/edit)
* [GitLab Runner daily triage](https://gitlab.com/gitlab-org/quality/triage-ops/pipeline_schedules/29681/edit)
* [GitLab QA daily triage](https://gitlab.com/gitlab-org/quality/triage-ops/pipeline_schedules/29050/edit)
* [Gitlab-Org Group add stage and group labels to open issues and merge requests](https://gitlab.com/gitlab-org/quality/triage-ops/pipeline_schedules/30002/edit)
* [Gitlab-Org Group label community contributions](https://gitlab.com/gitlab-org/quality/triage-ops/pipeline_schedules/11219/edit)
* [charts group daily triage](https://gitlab.com/gitlab-org/quality/triage-ops/pipeline_schedules/19768/edit)
* [charts group add stage and group labels to open issues and merge requests](https://gitlab.com/gitlab-org/quality/triage-ops/pipeline_schedules/31519/edit)
* [www-gitlab-com daily schedule](https://gitlab.com/gitlab-org/quality/triage-ops/pipeline_schedules/18297/edit)
* [customers-gitlab-com schedule to groom old issues](https://gitlab.com/gitlab-org/quality/triage-ops/pipeline_schedules/34776/edit)
* [license-gitlab-com schedule to groom old issues](https://gitlab.com/gitlab-org/quality/triage-ops/pipeline_schedules/34777/edit)

#### Daily schedules for adding project specific default labels

* [gitter](https://gitlab.com/gitlab-org/quality/triage-ops/pipeline_schedules/31878/edit)
* [build/CNG](https://gitlab.com/gitlab-org/quality/triage-ops/pipeline_schedules/31879/edit)
* [omnibus-gitlab](https://gitlab.com/gitlab-org/quality/triage-ops/pipeline_schedules/31880/edit)
* [gitlab-runner](https://gitlab.com/gitlab-org/quality/triage-ops/pipeline_schedules/31937/edit)
* [gitaly](https://gitlab.com/gitlab-org/quality/triage-ops/pipeline_schedules/31938/edit)

### Triage operations schedules (weekly)

* [CE weekly triage](https://gitlab.com/gitlab-org/quality/triage-ops/pipeline_schedules/11986/edit)
* [EE weekly triage](https://gitlab.com/gitlab-org/quality/triage-ops/pipeline_schedules/11987/edit)
* [Gitaly weekly triage](https://gitlab.com/gitlab-org/quality/triage-ops/pipeline_schedules/29055/edit)
* [GitLab Runner weekly triage](https://gitlab.com/gitlab-org/quality/triage-ops/pipeline_schedules/29680/edit)
* [GitLab QA weekly triage](https://gitlab.com/gitlab-org/quality/triage-ops/pipeline_schedules/29051/edit)

## Technical documentation

This section contains some guidance on how to do some current steps to test and setup new triage pipelines.

### Testing with a dry-run

When creating or changing a policy, you are able to test it by leveraging the pipelines within this project.

#### Preconditions

1. If you are changing or creating a new policy, be sure to open your merge request first as WIP.
1. Ensure there is data available for your test condition

#### Steps

1. Hover over CI / CD in the sidebar
1. Select Pipelines
1. Click "Run Pipeline"
1. Select your branch for your merge request you'd like to test
1. Enter a variable called `TRIAGE_POLICY_FILE` to select the policy file you want to test, for example: `policies/label-missed-slo.yml`
1. Click "Run Pipeline"
1. All defaults in `bin/setup.sh` will apply and the policy file will run in the `dry-run:custom` job of the pipeline against `TRIAGE_SOURCE_PATH` (which by default is `13083`, the ID of the `gitlab-org/gitlab-ce` project).
1. You can find your pipeline you just triggered from your merge request as well.

### Setting up a new pipeline schedule

Creating the scheduled pipeline can be done when the merge request to implement a new triage package is merged.

1. Navigate to the CI/CD -> Schedules page.
1. Click the `New schedule` button.
1. Enter the pipeline details. Ensure the project is listed in the pipeline description. Use CRON syntax to schedule the jobs such as: `0 0 * * 1-5` for midnight Monday - Friday or `0 0 * * 1` for midnight on Monday. [Cron Guru](https://crontab.guru/) can help you come up with the desired cron format.
1. Add the following pipeline variables:
  1. `TRIAGE_SOURCE_TYPE` - Set to either `projects` or `groups`.
  1. `TRIAGE_SOURCE_PATH` - This is the project repository which is being setup for triage. For example, GitLab CE would be `gitlab-org/gitlab-ce` since the project is <https://gitlab.com/gitlab-org/gitlab-ce>.
     **We recommend using the project ID instead of its path in case the project is renamed.**
  1. Set one of the following triaging type: (only pick one)
     1. `DAILY_AUTOMATION` - Set value to `1` to run all jobs with the `.daily-automation` anchor in [the pipeline](https://gitlab.com/gitlab-org/quality/triage-ops/blob/master/.gitlab-ci.yml).
     1. `WEEKLY_AUTOMATION` - Set value to `1` to run all jobs with the `.weekly-automation` anchor in [the pipeline](https://gitlab.com/gitlab-org/quality/triage-ops/blob/master/.gitlab-ci.yml).
     1. `RUN_SINGLE` - Set value to `1` to run a single policy file in the `single-run` stage.
  1. `TRIAGE_POLICY_FILE` (if applicable) - A specific [policy file](https://gitlab.com/gitlab-org/quality/triage-ops/tree/master/policies) for the pipeline. This is used only when `RUN_SINGLE` is set.
  1. `UNLABELLED_TRIAGE_PACKAGE` (if applicable) - This will run the [unlabelled-issues](https://gitlab.com/gitlab-org/quality/triage-ops/blob/master/policies/unlabelled-issues.yml) policy in the pipeline
  1. `COMMUNITY_TRIAGE_PACKAGE` (if applicable) - This will run the [community-merge-requests](https://gitlab.com/gitlab-org/quality/triage-ops/blob/master/policies/community-merge-requests.yml) policy in the pipeline.
1. Add a link to the pipeline in the Triage-Ops README.md.
1. Activate the schedule **after the merge request for the triage package is complete**.
